Prerrequisitos
Una vez clonado el repositorio debes tener instalado:
EL IDE IntelliJ IDEA
JDK JAVA 11
XAMPP PARA EL MOTOR DE BASE DE DATOS MYSQL





Instrucciones para Ejecutar el Reto Tecnico
1. Clonar El Repositorio. https://gitlab.com/avegam1/sofkaureto
2. crear la base de datos en el phpmyadmin ya que mysql es el motor de base de datos utilizado en el proyecto, para esto Se debe ubicar el script  con nombre "bd.sql" que se encuentra en la carpeta de conexion del proyecto.
3. Ahora dentro del proyecto, se debe agregar el driver mysql conector   ubicado en la carpeta conexion ya que sin el driver  genera un error, para agregarlo al proyecto mirar el siguiente link https://www.youtube.com/watch?v=h6xwRwlFypM&t=176s
4. Abrir la interfaz Propiedades ubicada en la carpeta conexion, para configurar los  los datos de acceso al servidor de phpmyadmin enn mi caso el  username="root", password=""
5. Finalmente, en el la carpeta main del proyecto, se encuentra el archivo principal el cual se debe ejecutar.
